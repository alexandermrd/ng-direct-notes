'use strict';
 
describe('contactapp', function(){
    beforeEach(angular.mock.module('NotesFilters'));

    describe('reducer', function(){
    	it("Counting and sorting",  inject(function(reducerFilter){
                var res = reducerFilter(['a', 'b', 'a']);
    		    expect(res[0].count).toBe(2);
                expect(res[0].name).toBe('a');

                expect(res[1].count).toBe(1);
                expect(res[1].name).toBe('b');
        }));

        it("Limiter",  inject(function(reducerFilter){
                var res = reducerFilter(['alef', 'b', 'alef','c','alef', 'd', 'b', 'd'], 3);
                expect(res.length).toBe(3);
                expect(res[2].name).toBe('d');
        }));

    });
});
