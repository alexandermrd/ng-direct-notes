var NotesDirectives = angular.module('NotesDirectives',[]);


getRange = function(testData){
    var allCourses = [];
    for (var x = 0; x < testData.length; x++){
        for (var y = 0; y < testData[x].courses.length; y++){
            allCourses.push(testData[x].courses[y]);
        }
    }
    // console.log(allCourses);
    var dRange = [d3.min(allCourses,function(d){
                        return d3.time.day.floor(new Date(d.start))}), 
                    d3.max(allCourses,function(d){
                        return d3.time.day.ceil(new Date(d.end))})];     
    return dRange;
};

NotesDirectives.directive('mntTimeBar', [function () {
    return {
        restrict: 'E',
        link: function ($scope, iElement, iAttrs) {
          dRange = getRange($scope.testData);
          console.log("Range", dRange); 
            var selection = d3.selectAll(iElement);

            var x = d3.time.scale()
            .domain(getRange($scope.testData))
            .rangeRound([20, 800]);
 
            var xAxis = d3.svg.axis()
            .scale(x)
            .tickFormat(d3.time.format('%b %d'))
            .tickSize(4)
            .tickPadding(8)
            ;

            var svgContainer = selection.append("svg")
             .attr("width", "840px")             
             .attr("height", "64px")
             ;

             svgContainer.append('g')
                 .attr('class', 'x axis')      
                 .call(xAxis);

             svgContainer.append('line')
             .attr("x1", x(new Date()))             
             .attr("y1", "0")
             .attr("x2", x(new Date()))             
             .attr("y2", "40")       
             .attr("stroke", "#ff0000")
             .attr("stroke-width", 1)      
             ;
        }
    };
}])

function addGradient(gradients, name, colorStart, colorEnd){
        gradient = gradients.append("svg:linearGradient")
        .attr("id", name)
        .attr("x1", "0%")
        .attr("y1", "50%")
        .attr("x2", "100%")
        .attr("y2", "50%")
        .attr("spreadMethod", "pad");

        gradient.append("svg:stop")
            .attr("offset", "0%")
            .attr("stop-color", colorStart)
            .attr("stop-opacity", 1);

        gradient.append("svg:stop")
            .attr("offset", "100%")
            .attr("stop-color", colorEnd)
            .attr("stop-opacity", 1);  
};

NotesDirectives.directive('mntScheduleGraph', function($timeout){
    'use strict';
    return {
        // templateUrl:'templates/alEditableSpan.html',
        link:function($scope, element, attr){         
            var selection = d3.selectAll(element);
            var graphData = $scope.testData[attr.index].courses;

            var x = d3.time.scale()
            .domain(getRange($scope.testData))
            .rangeRound([20, 800]);
            console.log("Current date", x(new Date()));

        
            var svgContainer = selection.append("svg")
             .attr("width", "840px")             
             .attr("height", "64px")
             ;

            var gradients = svgContainer.append("svg:defs");
            addGradient(gradients, "completed", "#00aa00", "#00ee00");
            addGradient(gradients, "inprogress", "#F0E68C", "#ADFF2F");
            addGradient(gradients, "planned", "#0000aa", "#0000ee");
            

             var circles = svgContainer.selectAll("rect").
             data(graphData).enter()   
             .append("rect")
             .attr("height", 64)
             .attr("width", function(d)
             {
                console.log("Start for ", d, x(new Date(d.start)));
                console.log("Stop for ", d, x(new Date(d.end)));
                return x(new Date(d.end)) - x(new Date(d.start));
                
             })
             .attr("x", function(d){                
                return x(new Date(d.start));})
             .style("fill", function(d){                
                return "url(#"+ d.status + ")";});        
        }
    };
});