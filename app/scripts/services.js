var noteServices = angular.module('noteResource', ['ngResource']);

noteServices.factory('lazyService', function($q, $timeout){
    'use strict';
    var ls =
    {
        sleeper:function(){
            console.log('sleeper activated');
            var delay = $q.defer();
            $timeout(delay.resolve, 1000);
            return delay.promise;
        }
    };
    return ls;
});


noteServices.factory('mentiesService', ['$resource',
  function($resource){
    return $resource('http://10.27.160.14:8080/schmetterlling/Courses.groovy', {}, {
      query: {method:'GET', params:{}, isArray:true}
    });
  }]);


