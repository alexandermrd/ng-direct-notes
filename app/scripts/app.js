var psiApp = angular.module('contactapp', ['ngRoute', 'NotesControllers', 'noteResource', 'NotesFilters']);


psiApp.config(['$routeProvider',
	function ($routeProvider) {
		'use strict';
		$routeProvider.
		when('/list/:page?', {
			templateUrl: 'partials/List.html',
			controller: 'ListController',
			resolve:{
				appData: function(lazyService){
					return lazyService.sleeper();
				}
			}
		}).
		when('/otherpage', {
			templateUrl: 'partials/SecondPage.html',
			controller: 'SecondPageController',
			resolve:{
				appData: function(lazyService){
					return lazyService.sleeper();
				}
			}
		}).
		otherwise({
			redirectTo: '/list'
		});
	}]);


psiApp.run(['$rootScope', function($root) {
	'use strict';
	$root.routeChanging = false;

	$root.$on('$routeChangeStart', function(e, curr, prev) {
		if (curr.$$route && curr.$$route.resolve) {
			$root.routeChanging = true;
		}
	});

	$root.$on('$routeChangeSuccess', function(e, curr, prev) {
		$root.routeChanging = false;
	});
}]);