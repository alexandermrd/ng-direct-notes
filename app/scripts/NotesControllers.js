var NotesControllers = angular.module('NotesControllers', ['NotesDirectives','ngMaterial','noteResource']);

NotesControllers.controller('ListController', ['$scope', '$location',
    function ($scope, $location) {
        'use strict';
        $scope.notes = [];
        $scope.availableTags = ['beta', 'zeta', 'gamma'];
        $scope.acSource = 'availableTags';
        $scope.reducerSrc = '2,4,2,a,c,a'

        $scope.addNote = function(){
            $scope.notes.push({name:'', count:'', tags:[]});
        };

        $scope.addTagToAutocompletes = function(){
            console.log('adding tag');
            var newVal = $scope.availableTags.slice();
            newVal.push('a');
            $scope.availableTags.push('a');// = newVal;
            console.log($scope.availableTags);
        };

        $scope.gotoSecondPage = function(){
            $location.path('/otherpage');
        };
    }
]);

NotesControllers.controller('SecondPageController', ['$scope', '$location', '$routeParams',
    function ($scope, $location, $routeParams) {
        'use strict';
        $scope.gotoIndex = function(){
            $location.path('/list');
        };
    }
]);

 NotesControllers.controller('MentoCtrl', function ($scope, $interpolate, $mdDialog, mentiesService) {
    var tabs = [
      { title: 'Active menthoring', active: true},
      { title: 'Mentors', active: false, content: "MentorList"},
      { title: 'Best Courses', active: false, disabled: false, content: "Best courses"}
    ];

    var mentors = [
        {"who": "Mykhaylo Lazor", "what" : "200 / year", "face":"https://upsa.epam.com/workload/photo/4000600100000187112"},
        {"who": "Stepan Mitish", "what" : "mentored 5 person for last year", "face":"https://upsa.epam.com/workload/photo/4000741400001128764"}
    ];

    var courses = [
        {"who": "BDDChecklists_Test Coverage", "what" : "23 attendies (87%) with A+ rating",
        "face" : "http://blog.bughuntress.com/wp-content/uploads/2013/09/bdd-cycle-around-tdd-cycles1-250x250.png"},
        {"who": "SDLC", "what" : "13 attendies (71%) with A+ rating",
        "face": "http://www.training-specialists.com/Images/SDLC.gif"}
    ];

    $scope.tabs = tabs;
    $scope.mentors = mentors;
    $scope.predicate = "title";
    $scope.reversed = true;
    $scope.courses = courses;
    $scope.selectedIndex = 0;
    $scope.allowDisable = true;

    $scope.onTabSelected = onTabSelected;

    $scope.testData = [{
        "name" : "Filip Nizar",
        "primarySkill" : "Java by Team",
        "photoUrl" : "https://upsa.epam.com/workload/photo/4060741400006402448", 
        "courses":[
            {"title":"Java.ext", "start":"10\/27\/2014", "end":"11\/3\/2014", "status": "completed"},
            {"title":"Java.ext", "start":"11\/6\/2014", "end":"11\/12\/2014", "status": "inprogress"},
            {"title":"Java.ext", "start":"11\/20\/2014","end":"11\/28\/2014", "status": "planned"}
        ] 
      },
        {
        "name" : "Ridwan Berhanu",
        "primarySkill" : "MBA by SomeOne2",
        "photoUrl" : "https://upsa.epam.com/workload/photo/4000741400000532785", 
        "courses":[
            {"title":"Java.ext", "start":"9\/27\/2014", "end":"10\/3\/2014", "status": "completed"},
            {"title":"Java.ext", "start":"11\/1\/2014", "end":"11\/14\/2014", "status": "inprogress"}
        ] 
      },
        {
        "name" : "Stepan Mitish",
        "primarySkill" : "Java by SomeOne",
        "photoUrl" : "https://upsa.epam.com/workload/photo/4060741400005803559", 
        "courses":[
            {"title":"Java.ext", "start":"10\/12\/2014", "end":"10\/22\/2014", "status": "completed"},
            {"title":"Java.ext", "start":"11\/16\/2014", "end":"12\/16\/2014", "status": "planned"}
        ] 
      }
    ];

   // $scope.testData = [ { "name": "\u0412\u0430\u0441\u044F \u041F\u0443\u043F\u043A\u0438\u043D", "primarySkill": "Java, \u0440\u0443\u043A\u043E\u0432\u043E\u0434\u0438\u0442\u0435\u043B\u044C: Andrey Ignatenko", "photoUrl": "https://upsa.epam.com/workload/photo/4000741400000532785", "courses": [ { "start": "2014-01-01T03:00:00+0000", "end": "2014-02-01T03:00:00+0000", "status": "completed" } ] }, { "name": "\u041E\u043B\u0435\u0433 \u0412\u0430\u0441\u0435\u0447\u043A\u0438\u043D", "primarySkill": "Java, \u0440\u0443\u043A\u043E\u0432\u043E\u0434\u0438\u0442\u0435\u043B\u044C: Andrey Klimenko", "photoUrl": "https://upsa.epam.com/workload/photo/4060741400008762639", "courses": [ { "start": "2015-01-01T03:00:00+0000", "end": "2015-02-01T03:00:00+0000", "status": "planned" } ] }, { "name": "\u0412\u0430\u043B\u0435\u0440\u0438\u0439 \u0421\u044E\u0442\u043A\u0438\u043D", "primarySkill": ".NET, \u0440\u0443\u043A\u043E\u0432\u043E\u0434\u0438\u0442\u0435\u043B\u044C: Andrey Kaganovich", "photoUrl": "https://upsa.epam.com/workload/photo/4000741400003952673", "courses": [ { "start": "2014-11-01T03:00:00+0000", "end": "2015-12-01T03:00:00+0000", "status": "inprogress" } ] }, { "name": "\u041C\u0438\u0445\u0430\u0438\u043B \u0411\u043E\u044F\u0440\u0441\u043A\u0438\u0439", "primarySkill": "Java, \u0440\u0443\u043A\u043E\u0432\u043E\u0434\u0438\u0442\u0435\u043B\u044C: Denis Sokolov", "photoUrl": "https://upsa.epam.com/workload/photo/4060741400008570224", "courses": [ { "start": "2015-01-01T03:00:00+0000", "end": "2015-02-01T03:00:00+0000", "status": "planned" } ] }, { "name": "\u0421\u0442\u0435\u0444\u0430\u043D\u0438\u0434\u0430 \u041F\u0435\u0442\u0440\u043E\u0432\u0430", "primarySkill": ".NET, \u0440\u0443\u043A\u043E\u0432\u043E\u0434\u0438\u0442\u0435\u043B\u044C: Mariya Davydova", "photoUrl": "https://upsa.epam.com/workload/photo/4060741400006267624", "courses": [ { "start": "2014-02-01T03:00:00+0000", "end": "2014-03-01T03:00:00+0000", "status": "completed" } ] }, { "name": "\u0412\u044F\u0447\u0435\u0441\u043B\u0430\u0432 \u041A\u043E\u0440\u0435\u043F\u0430\u043D\u043E\u0432", "primarySkill": ".NET, \u0440\u0443\u043A\u043E\u0432\u043E\u0434\u0438\u0442\u0435\u043B\u044C: Ivan Kaledin", "photoUrl": "https://upsa.epam.com/workload/photo/4060741400006620410", "courses": [ ] } ] ; 
    $scope.testData = mentiesService.query();

    $scope.addTab = function (title, view) {
      var style = tabs[(tabs.length % 4)].style;
      view = view || title + " Content View";
      tabs.push({ title: title, content: view, active: false, disabled: false, style:style});
    };

    $scope.removeTab = function (tab) {
      for (var j = 0; j < tabs.length; j++) {
        if (tab.title == tabs[j].title) {
          $scope.tabs.splice(j, 1);
          break;
        }
      }
    };

    $scope.addKitten = function(){
        console.log("adding kitten");
        $scope.courses.push({
            "who": "BDDChecklists_Test Coverage", "what" : "23 attendies (87%) with A+ rating",
            "face" : "http://www.randomkittengenerator.com/images/cats/rotator.php"});
    };

    $scope.dialogAdvanced = function(ev) {
      $mdDialog.show({
        templateUrl: 'addUserDialog.tmpl.html',
        targetEvent: ev,
        controller: DialogController,
        onComplete:function(){
        }
      }).then(function(answer) {
        
        var courseDate = new Date();
        var courseDateEnd = new Date();
        courseDateEnd.setDate(courseDate.getDate() + 1);

        $scope.testData.push({        
            "name" : answer.split("@")[0].replace('_', ' '),
            "primarySkill" : "Java",
            "photoUrl" : "http://lorempixel.com/output/people-q-c-96-87-7.jpg",
            "courses" :[{"title":"Mocking in hackaton", "start": courseDate, "end": courseDateEnd , "status": "inprogress"}]
            });
      }, function() {
      });
    };
    ;

    // **********************************************************
    // Private Methods
    // **********************************************************

    function onTabSelected(tab) {
      $scope.selectedIndex = this.$index;
    }
  });


function DialogController($scope, $mdDialog) {
  $scope.email = "";
  $scope.hide = function() {
    $mdDialog.hide();
  };

  $scope.cancel = function() {
    $mdDialog.cancel();
  };

  $scope.answer = function(answer) {
    if ($scope.email){
        $mdDialog.hide($scope.email);
    }
  };
}