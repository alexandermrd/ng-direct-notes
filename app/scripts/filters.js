var NotesFilters = angular.module('NotesFilters',[]);

NotesFilters.filter('reducer', function(){
    'use strict';
    return function(input, limit){
        var countMap = {};
        var arrayResult = [];

        angular.forEach(input, function(el){
            countMap[el] = countMap[el] ? countMap[el] + 1 : 1;
        });

        for (var els in countMap) {
            arrayResult.push({'count':countMap[els], 'name': els });
        }

        arrayResult.sort(function(a,b){
            return b.count - a.count;
        });
        if (limit){
            arrayResult.splice(limit, arrayResult.length - limit);
        }
        return arrayResult;
    };
});